package com.kenfogel.bankroll;

import java.text.NumberFormat;

/**
 * A class that keeps track of the money in a casino game
 *
 * @author Ken Fogel
 */
public class Bankroll {
    
    private double money;
    
    public Bankroll() {
        this(0.0);
    }
    
    public Bankroll(double money) {
        super();
        this.money = money;        
    }
    
    public double getMoney() {
        return this.money;
    }
    
    public void addMoney(double winnings) {
        this.money += winnings;
    }
    
    public void subtractMoney(double losings) {
        this.money -= losings;
    }
    
    public String getFormattedMoney() {
     //   NumberFormat nf = NumberFormat.getCurrencyInstance();
       // return nf.format(money);
        
        return NumberFormat.getCurrencyInstance().format(this.money); //fluent
    }
}
