package com.kenfogel.bankroll;

/**
 * Tester for the bankroll
 * @author Ken Fogel
 */
public class MoneyTester {
    
    public void perform() {
        Bankroll bankroll1 = new Bankroll();
        bankroll1.addMoney(100.0);
        System.out.println("Current value: " + bankroll1.getFormattedMoney());
        bankroll1.subtractMoney(51.0);
        System.out.println("Current value: " + bankroll1.getFormattedMoney());
   
        Bankroll bankroll2 = new Bankroll(500.0);
        bankroll2.addMoney(100.0);
        System.out.println("Current value: " + bankroll2.getFormattedMoney());
        bankroll2.subtractMoney(51.0);
        System.out.println("Current value: " + bankroll2.getFormattedMoney());
    
    
    }
    
    public static void main(String[] args) {
        MoneyTester moneyTester = new MoneyTester();
        moneyTester.perform();
        System.exit(0);
        
    }
    
}
